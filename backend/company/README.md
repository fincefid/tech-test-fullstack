### Candidate Notes
I made the following assumptions while implementing the test 

- Pipeline was failing due to missing gradle jar. I fixed that as a first thing then frontend module is failed due to missing yarn as we have java image for Bitbucket pipeline. Ignored that failure as I chose to implement backend
- CompanyRequest has a required field called `name` in the api.yaml. Assuming it supposed to be `companyName`
- `/interview/v0` is part of the deployment not the api itself 
- I don't need to implement get side of things, so I didn't put any state to CompanyDao
- Also tested via Postman as well as tests
- I committed more frequent than usual to show you how I drove the design by the tests