package com.sedex.connect

import io.ktor.http.*
import io.ktor.http.ContentType.*
import io.ktor.server.testing.*
import io.mockk.every
import io.mockk.mockk
import java.util.*
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull

class ApplicationTest {

    var mockCompanyDao = mockk<CompanyDao>()

    @Test
    fun companyPost_Success() {
        every { mockCompanyDao.save(any()) } returns aCompany

        withTestApplication({ webModule(dao = mockCompanyDao, testing = true) }) {
            // full request body
            handleRequest(HttpMethod.Post, "/company") {
                addHeader("Content-Type", Application.Json.toString())
                setBody(requestBody) }.apply {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals(expectedResponseBody, response.content)
            }

            // with minimal requestBody
            handleRequest(HttpMethod.Post, "/company") {
                addHeader("Content-Type", Application.Json.toString())
                setBody(minimalValidRequestBody) }.apply {
                assertEquals(HttpStatusCode.OK, response.status())
            }
        }
    }

    // Although API does not have response body for non-200 response, i am implementing the following test case due to `required: true` in requestBody sections
    @Test
    fun companyPost_ValidationErrors() {
        withTestApplication({ webModule(dao = mockCompanyDao, testing = true) }) {
            // with missing request body
            handleRequest(HttpMethod.Post, "/company") {
                addHeader("Content-Type", Application.Json.toString()) }.apply {
                assertEquals(HttpStatusCode.BadRequest, response.status())
                assertNull(response.content)
            }

            // with missing companyName
            handleRequest(HttpMethod.Post, "/company") {
                addHeader("Content-Type", Application.Json.toString())
                setBody(missingMandatoryDataRequestBody) }.apply {
                assertEquals(HttpStatusCode.BadRequest, response.status())
                assertNull(response.content)
            }
        }
    }

    private val aCompany = Company(
        UUID.fromString("99bc248e-2b7f-11eb-adc1-0242ac120002"),
        "companyName",
        "companyType",
        "natureofBusiness",
        "incorporatedDate",
        "emailAddress",
        "phoneNumber",
        CompanyAddress("address.addressLine1", "address.addressLine2", "address.city", "address.state", "address.postalCode", "address.countryCode"),
        "2020-11-23T19:11:23.966305",
        "2020-11-23T19:11:23.966305")

    private val requestBody = """
            |{
            |   "companyName": "companyName",
            |   "companyType": "companyType",
            |   "natureofBusiness": "natureofBusiness",
            |   "incorporatedDate": "incorporatedDate",
            |   "emailAddress": "emailAddress",
            |   "phoneNumber": "phoneNumber",
            |   "address": {
            |       "addressLine1": "address.addressLine1",
            |       "addressLine2": "address.addressLine2",
            |       "city": "address.city",
            |       "state": "address.state",
            |       "postalCode": "address.postalCode",
            |       "countryCode": "address.countryCode"
            |   }
            |}
        """.trimMargin()

    private val expectedResponseBody = """
            |{
            |  "id" : "99bc248e-2b7f-11eb-adc1-0242ac120002",
            |  "companyName" : "companyName",
            |  "companyType" : "companyType",
            |  "natureofBusiness" : "natureofBusiness",
            |  "incorporatedDate" : "incorporatedDate",
            |  "emailAddress" : "emailAddress",
            |  "phoneNumber" : "phoneNumber",
            |  "address" : {
            |    "addressLine1" : "address.addressLine1",
            |    "addressLine2" : "address.addressLine2",
            |    "city" : "address.city",
            |    "state" : "address.state",
            |    "postalCode" : "address.postalCode",
            |    "countryCode" : "address.countryCode"
            |  },
            |  "createdTime" : "2020-11-23T19:11:23.966305",
            |  "updatedTime" : "2020-11-23T19:11:23.966305"
            |}
        """.trimMargin()

    private val minimalValidRequestBody = """
            |{
            |   "companyName": "companyName"
            |}
        """.trimMargin()

    private val missingMandatoryDataRequestBody = """
            |{
            |   "companyType": "companyType"
            |}
        """.trimMargin()
}
