package com.sedex.connect

import java.time.LocalDateTime
import java.util.*

object CompanyDao {
    fun save(company: CompanyRequest) = company.toCompany(UUID.randomUUID(), LocalDateTime.now().toString())
}

data class Company(
    val id: UUID,
    val companyName: String,
    val companyType: String?,
    val natureofBusiness: String?,
    val incorporatedDate: String?,
    val emailAddress: String?,
    val phoneNumber: String?,
    val address: CompanyAddress?,
    val createdTime: String?,
    val updatedTime: String?
)
data class CompanyAddress(
    val addressLine1: String,
    val addressLine2: String,
    val city: String,
    val state: String,
    val postalCode: String,
    val countryCode: String
)

fun CompanyRequest.toCompany(companyId: UUID, currentTime: String) = Company(
    companyId,
    companyName,
    companyType,
    natureofBusiness,
    incorporatedDate,
    emailAddress,
    phoneNumber,
    address,
    currentTime,
    currentTime
)