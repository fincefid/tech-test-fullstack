package com.sedex.connect

import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.databind.SerializationFeature
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.jackson.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

fun Application.module(testing: Boolean = false) {
    webModule( dao = CompanyDao, testing = testing)
}

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.webModule(dao: CompanyDao, testing: Boolean = false) {
    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
        }
    }

    install(StatusPages) {
        exception<JsonMappingException> {
            call.respond(HttpStatusCode.BadRequest)
        }
    }

    routing {
        post("/company") {
            val companyRequest = call.receive<CompanyRequest>()
            val companyResponse = dao.save(companyRequest)
            call.respond(companyResponse)
        }
    }
}

data class CompanyRequest(
    val companyName: String,
    val companyType: String?,
    val natureofBusiness: String?,
    val incorporatedDate: String?,
    val emailAddress: String?,
    val phoneNumber: String?,
    val address: CompanyAddress?)
